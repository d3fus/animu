FROM rustlang/rust:nightly

WORKDIR /usr/src/animu
COPY . .

RUN cargo install --path .

CMD ["animu"]
