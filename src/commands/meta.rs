#![feature(rustc_private)]
extern crate reqwest;
extern crate base64;
use std::collections::HashMap;
use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
    Args,
};


#[command]
fn ping(ctx: &mut Context, msg: &Message) -> CommandResult {
    let _ = msg.channel_id.say(&ctx.http, "Pong!");

    Ok(())
}

#[command]
fn test(ctx: &mut Context, msg: &Message) -> CommandResult {
  let _ = msg.channel_id.say(&ctx.http, "test");

  Ok(())
}

#[command]
fn help(ctx: &mut Context, msg: &Message) -> CommandResult {
  let _msg = msg.channel_id.send_message(&ctx.http, |m| {
    m.embed(|e| {
      e.title("Animu help");
      e.field("~animu {url}", "attach a image to the message or paste a url to the image behinde the animu command", false);
      e.field("~ping", "returns pong", false);

      e
    });

    m
  });

  Ok(())
}

#[command]
fn animu(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult { 
  //println!("test");
  let mut img_url: String = "".to_string();
  if args.len() > 0 {
    img_url = args.single::<String>().unwrap();
    //println!("{}", img_url);
  } else {
    img_url = msg.attachments[0].proxy_url.to_string();
  }
  println!("{}", img_url);
  let client = reqwest::Client::new();
  let mut img = client.get(&img_url).send()?;
  //println!("{}", img);
  let mut buf: Vec<u8> = vec![];
  img.copy_to(&mut buf);
  let img_b64 = base64::encode(&buf);
  let mut data = HashMap::new();
  let mut header = reqwest::header::HeaderMap::new();
  data.insert("image", img_b64);
  header.insert("Content-Type", "application/json".parse().unwrap());
  //println!("{}", data["image"]);

  let mut resp = client.post("https://trace.moe/api/search")
    .headers(header).json(&data).send()?;


  let le: serde_json::Value = serde_json::from_str(&resp.text()?)?;

  let re = &le["docs"][0];
  let msg = msg.channel_id.send_message(&ctx.http, |m| {
    m.embed(|e| {
      e.title("Animu anime search");
      e.fields(vec![
        ("Anime name", &re["title_romaji"], true),
        ("Anime name english", &re["title_english"], true),
      ]);
      e.field("Similar to picture", &re["similarity"], false);
      e.field("Episode", &re["episode"], false);
      e.field("Anilist link", 
        format!("https://anilist.co/anime/{}", re["anilist_id"]), false);
      e.field("MAL link", 
        format!("https://myanimelist.net/anime/{}", re["mal_id"]), false);
      e.footer(|f| {
        f.text("Powert by https://trace.moe");

        f
      });

      e
    });
    m
  });

  if let Err(why) = msg {
      println!("Error sending message: {:?}", why);
  }

  //if let Err(why) = msg {
  //  println!("Error sending message: {:?}", why);
  //}

  Ok(())
}
